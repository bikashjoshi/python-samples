# list
list = [1, 2, 3]
list.append(4)
print(list, "is ", type(list))

for item in list:
    print(item)

for i, item in enumerate(list):
    print("item at index {0} is {1}".format(i, item))

# slicing and negative index
# list[-n] is list[0]
# list[-1] is list[n-1]
# list[-0] is list[0]

y = list[0:-1]
print("Sliced[0:-1] is {0}".format(y))

z = list[1: -2]
print("Sliced[1:-2] is {0}".format(z))
 

 ## NO ARRAYS
 ## USE numpy

# set
a = {1, 2, 3, 2}
print(a, "is ", type(a))


# dictionary
print("---------------------")

dict = {"key1" : 1, "key2" : "some text."}
print(dict, "is ", type(dict))



