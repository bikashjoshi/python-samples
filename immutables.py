# int
a = 1
print(a, "is ", type(a))

# string
a = "test"
print(a, "is ", type(a))

# float
a = 2.5
print(a, "is ", type(a))

# tuple
a = (1, 2, 3, 2)
print(a, "is ", type(a))

