class NegativeValueException(Exception):
    def __init__(self, arg):
        self.args = arg

def area(x,y):
    if x < 0 or y < 0:
        raise NegativeValueException("Dimension cannot be negative.")
    return x*y


def divide(x,y):
    if y == 0:
        raise "Divisor cannot be a zero value."
    return x/y

try:
    result = area(2, 3)
    print(result)
except NegativeValueException as e:
    pass #swallows
finally:
    print("End of program..")


