def swap(x, y):
    return y,x

def sort(x, predicate):
    length = len(x)
    for i in range(0, length-1):
        for j in range(i, length):
            if predicate(x[i], x[j]):
                (x[i], x[j]) = swap(x[i], x[j]) # tuple unpacking


list=[10, 2, 1, 7, 5, 9]
print('Before: {0}'.format(list))
sort(list, lambda x, y: x > y)
print('After: {0}'.format(list))

people = [{"name" : "Joe", "age" : 17},{"name" : "Ted", "age" : 7}, {"name" : "Ryan", "age" : 22}]
print('Before: {0}'.format(people))
sort(people, lambda x, y: x["name"] > y["name"])
print('After: {0}'.format(people))