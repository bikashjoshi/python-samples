"""Retrieves stock price of provided symbols from Yahoo finance.
"""

from urllib.request import urlopen
from urllib.parse import urlencode
import json


def getPrice(symbols):
    """Gets price using yahoo finance api

    Args: Comma separated symbols with double quote.

    Returns: Quote object (or its array)
    """
    baseurl= "http://query.yahooapis.com/v1/public/yql?"
    query = {
    'q': 'select LastTradePriceOnly, Change, Symbol from yahoo.finance.quote where symbol in ('+ symbols+ ')',
    'format': 'json',
    'env': 'store://datatables.org/alltableswithkeys'
     }

    url = baseurl + urlencode(query)
    print(url)
    with urlopen(url) as results:
        response=results.read().decode('utf-8')
    jsonresponse = json.loads(response)
    query =jsonresponse["query"]
    results=query["results"]
    quote=results["quote"]
    return quote


def printPrice(quote):
    """Prints Symbol, LastTradePrice, Change from Quote object (or its array).

    Args: Quote object (single object or array)    
    """
    print("Symbol", "LastTradePriceOnly", "Change")
    if (type(quote) is list):
        for q in quote:
            print(q["Symbol"], q["LastTradePriceOnly"], q["Change"])
    else:
         print(quote["Symbol"], quote["LastTradePriceOnly"], quote["Change"])


def main():
    """Command prompt to get price from provided symbols.
    """
    symbols = input("User input:")
    print(symbols)
    jsonresponse=getPrice(symbols)
    printPrice(jsonresponse)


if (__name__ == "__main__"):
    main()


