from pymongo import MongoClient

connection = MongoClient("mongodb://localhost:27017")
db = connection.school
scores = db.scores


def find_score_by_id(id):
    query = {"_id": id}
    return scores.find_one(query)


def find_score_by_query(query):
    cursor = scores.find(query)
    for document in cursor:
        print(document)


def insert_score(score):
    query = {'score': score}
    return scores.insert(query)


def update_score(id, newScore):
    return scores.update({'_id': id}, {'$set': {'score': newScore}})


def delete_score(id):
    return scores.delete_one({'_id': id})


print("----------Inserting document first------------")
id = insert_score(21)
print("Inserted data Id {0}".format(id))

print("----------Finding document by Id--------------")
doc = find_score_by_id(id)
print(doc)

print("----------Finding document by Query------------")
find_score_by_query({"score" : {"$gt": 10}})

print("----------Updating score------------------------")
result = update_score(id, 23)
print(result)

print("----------Finding document by Id----------------")
doc = find_score_by_id(id)
print(doc)

print("----------Deleting document by Id---------------")
result = delete_score(id)
print(result)

print("----------Finding all documents by Query----------")
find_score_by_query({})